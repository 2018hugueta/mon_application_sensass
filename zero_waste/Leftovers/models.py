from django.db import models
from eat_with_me.models import Utilisateurs

# Create your models here.

class Reste(models.Model):
    nom_du_produit = models.CharField(max_length=400)
    date_peremption = models.DateField()
    date_limite_recuperation = models.DateTimeField()
    lieu = models.CharField(max_length=200)
    cuisinier = models.ForeignKey(Utilisateurs, on_delete=models.CASCADE)