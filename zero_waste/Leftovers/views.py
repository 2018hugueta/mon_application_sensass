from django.shortcuts import render
from Leftovers import models

# Create your views here.

def products_list(request):
    annonces = models.Reste.objects.values()
    context = {'Annonces' : annonces}
    return render(request, 'Leftovers/liste.html', context)