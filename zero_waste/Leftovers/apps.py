from django.apps import AppConfig


class LeftoversConfig(AppConfig):
    name = 'Leftovers'
