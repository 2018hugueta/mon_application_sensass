from django.shortcuts import render
from .models import ListeApplications

# Create your views here.

def index(request):
    applications = ListeApplications.objects.values()
    context = {'applications' : applications}
    return render(request, 'accueil/index.html', context)

