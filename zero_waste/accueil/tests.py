from django.urls import resolve
from django.test import TestCase
from eat_with_me.views import viewtest, liste, detail

from django.http import HttpResponse, HttpRequest

class viewtestpage(Request):
    def test_root_url_viewtest(self):
        found = resolve('/')
        self.assertEqual(found.func, viewtest)

class eat_with_me_liste(Request):
    def test_root_url_viewtest(self):
        found = resolve('/eat_with_me') 
        self.assertEqual(found.func,liste)

    def correct_html_viewtest(self):
        response = HttpRequest()
        response = liste(request)
        html = response.content.decode('utf8')
        message_liste = 'blabla'
        self.assertEqual(html, message_liste)    

class eat_with_me_detail(Request):

    def test_root_url_detail(self):
        found = resolve('/eat_with_me/1')
        self.assertEqual(found.func, detail)

    def correct_html_detail(self):
        response = HttpRequest()
        response = detail(request, 1)
        html = reponse.content.decode('utf8')
        message = '<p>Vous pouvez manger riz à la location 4CC4 avec Jean Jean.</p>'
        self.assertEqual(html, message) 

    def incorrect_html_detail(self):
        response = HttpRequest()
        response = detail (request, 2)
        html = response.content.decode('utf8')
        message_incorrect = 'blabla' 
        self.assertEqual(html, message_incorrect)   

        






# Create your tests here.
