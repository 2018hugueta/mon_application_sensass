from django.apps import AppConfig


class EatWithMeConfig(AppConfig):
    name = 'eat_with_me'
