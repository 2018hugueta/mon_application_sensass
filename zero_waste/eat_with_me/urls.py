from django.urls import path

from . import views

urlpatterns = [
    path('', views.liste, name='liste'),
    path('<int:annonce_id>/', views.detail, name='detail')
]
