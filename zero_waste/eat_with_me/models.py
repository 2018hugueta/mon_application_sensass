from django.db import models

# Create your models here.


class Utilisateurs(models.Model):
    nom = models.CharField(max_length=200)
    prenom = models.CharField(max_length=200)
    email = models.EmailField(max_length=400)
    numero = models.CharField(max_length=10)
    

class Annonces(models.Model):
    nom_du_plat = models.CharField(max_length=400)
    nb_de_places = models.IntegerField()
    lieu = models.CharField(max_length=200)
    cuisinier = models.ForeignKey(Utilisateurs, on_delete=models.CASCADE)




